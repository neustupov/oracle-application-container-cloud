package ru.neustupov.rest.controller;

import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.neustupov.rest.Employee;
import ru.neustupov.rest.repository.EmployeeRepository;

@CrossOrigin
@RestController
@RequestMapping("/employees")
public class EmployeeController {

  Logger logger = LogManager.getLogger(EmployeeController.class);

  private EmployeeRepository employeeRepository;

  public EmployeeController(EmployeeRepository employeeRepository) {
    this.employeeRepository = employeeRepository;
  }

  @GetMapping
  public List getAll() {
    logger.info("Get all employees");
    return employeeRepository.getAllEmployees();
  }

  @GetMapping(value = "/{id}")
  public ResponseEntity get(@PathVariable long id) {

    Employee match = employeeRepository.getEmployee(id);

    if (match != null) {
      return new ResponseEntity<>(match, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = "/lastname/{name}")
  public ResponseEntity getByLastName(@PathVariable String name) {

    List matchList = employeeRepository.getByLastName(name);

    if (matchList.size() > 0) {
      return new ResponseEntity<>(matchList, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

  }

  @PostMapping
  public ResponseEntity add(@RequestBody Employee employee) {
    if (employeeRepository.add(employee)) {
      return new ResponseEntity<>(null, HttpStatus.CREATED);
    } else {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping(value = "{id}")
  public ResponseEntity update(@PathVariable long id, @RequestBody Employee employee) {

    if (employeeRepository.update(id, employee)) {
      return new ResponseEntity<>(null, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping(value = "{id}")
  public ResponseEntity delete(@PathVariable long id) {

    boolean result = employeeRepository.delete(id);

    if (result) {
      return new ResponseEntity<>(null, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
  }
}
