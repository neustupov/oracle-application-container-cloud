package ru.neustupov.rest;

import java.util.concurrent.CopyOnWriteArrayList;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class MockEmployeeList {

  private static final CopyOnWriteArrayList<Employee> eList = new CopyOnWriteArrayList<>();

  static {
    PodamFactory podam = new PodamFactoryImpl();

    for (int i = 0; i < 10; i++) {
      Employee employee = podam.manufacturePojoWithFullData(Employee.class);
      eList.add(employee);
    }
  }

  private MockEmployeeList() {
  }

  public static CopyOnWriteArrayList<Employee> getInstance() {
    return eList;
  }
}
