package ru.neustupov.rest.repository;

import java.util.List;
import ru.neustupov.rest.Employee;

public interface EmployeeRepository {

  public List getAllEmployees();

  public Employee getEmployee(long id);

  public List getByLastName(String name);

  public List getByTitle(String title);

  public List getByDepartment(String department);

  public boolean add(Employee employee);  // False equals fail

  public boolean update(long id, Employee employee); // False equals fail

  public boolean delete(long id); // False equals fail
}
