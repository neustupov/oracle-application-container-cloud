package ru.neustupov.rest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Employee {

  private final long id;
  private final String firstName;
  private final String lastName;
  private final String email;
  private final String phone;
  private final String birthDate;
  private final String title;
  private final String department;

  public Employee(){
    super();
    id = 0;
    firstName = "";
    lastName = "";
    email = "";
    phone = "";
    birthDate = "";
    title = "";
    department = "";
  }

  public long getId() {
    return id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public String getPhone() {
    return phone;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getTitle() {
    return title;
  }

  public String getDepartment() {
    return department;
  }
}
